#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
	sorted[0]=toSort[0];
	int taille=1;
	for(int i=1;i<toSort.size();i++){
		int j=0;
		int tri=0;
		while(j<taille && tri==0){
			if(sorted[j]>=toSort[i]){
				sorted.insert(j,toSort[i]);
				tri++;
			}
			j++;
		}
		if(tri==0){
			sorted.insert(taille, toSort[i]);
		}
	taille++;

	}

	// insertion sort from toSort to sorted
	
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
