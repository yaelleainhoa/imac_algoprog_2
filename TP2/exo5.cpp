#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	if (origin.size()==1){
		return;
	}

	else{
		// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
	int i=0;
	for (i; i<first.size(); i++){
		first[i]=origin[i];
	}
	int j=0;
	for(i;i<origin.size();i++){
		second[j]=origin[i];
		j++;
	}

	// recursiv splitAndMerge of lowerArray and greaterArray
	splitAndMerge(first);
	splitAndMerge(second);

	// merge
	merge(first, second, origin);
	}
}

void merge(Array& first, Array& second, Array& result)
{
	int index_f=0;
	int index_s=0;
	for (int i=0; i<first.size()+second.size();i++){
		if(index_s==second.size()){
			result[i]=first[index_f];
			index_f++;
		}
		else if(index_f==first.size()){
			result[i]=second[index_s];
			index_s++;
		}
		else if (first[index_f]<=second[index_s]){
			result[i]=first[index_f];
			index_f++;
		}
		else{
			result[i]=second[index_s];
			index_s++;
		}
	}
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
