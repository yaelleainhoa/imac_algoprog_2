#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise_liste(Liste* liste)
{
    liste->premier=NULL;

}

bool est_vide_liste(const Liste* liste)
{
    if(liste->premier==NULL){
        return true;
    }
    return false;
}

void ajoute_liste(Liste* liste, int valeur)
{
    Noeud *noeud = new Noeud;
    if (noeud==NULL){
        std::cout << "Allocation echec"<<std::endl;
        exit(1);
    }
    noeud->donnee=valeur;
    noeud->suivant=liste->premier;
    liste->premier=noeud;
}

void affiche_liste(const Liste* liste)
{
    Noeud * actuel = liste->premier;
    while(actuel!=NULL){
        std::cout << actuel->donnee<< " ";
        actuel=actuel->suivant;

    }
    std::cout << endl;
}

int recupere_liste(const Liste* liste, int n)
{
    Noeud * actuel = liste->premier;
    int i=1;
    while(actuel->suivant!=NULL && i<=n){ //Cette méthode ne renvoie pas la n-ième valeur mais la n+1 mais 
    //comme l'énoncé demande la 5ème valeur pour n=4 je l'ai programmé de cette manière
    //Si l'on veut une fonction qui renvoie la nème valeur il faut remplacer "i<=n" par "i<n"
        actuel=actuel->suivant;
        i++;
    }
    return actuel->donnee;
}

int cherche_liste(const Liste* liste, int valeur)
{
    
    Noeud * actuel = liste->premier;
    int i=1;
    while(actuel!=NULL){
        if (actuel->donnee==valeur){
            return i;
        }
        i++;
        actuel=actuel->suivant;
    }
    return -1;
}

void stocke_liste(Liste* liste, int n, int valeur)
{
    Noeud * actuel = liste->premier;
    int i=1;
    while(actuel!=NULL&&i<n){
        i++;
        actuel=actuel->suivant;
    }
    actuel->donnee=valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->capacite==tableau->taille){
        tableau->capacite+=5;
    }
    tableau->donnees=(int*)realloc(tableau->donnees, (tableau->taille+1)*sizeof(int));
    if (tableau->donnees==NULL){
         std::cout << "Allocation failed"<<endl;
        exit(1);
    }
    tableau->donnees[tableau->taille]=valeur;
    tableau->taille++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees=(int*)malloc(sizeof(int));
    tableau->taille=0;
    tableau->capacite=capacite;
    if (tableau->donnees==NULL){
        std::cout << "Allocation failed"<<std::endl;
        exit(1);
    }
}

bool est_vide(const DynaTableau* liste)
{
    if (liste->taille==0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->taille;i++){
        std::cout << tableau->donnees[i]<< " ";
    }
    std::cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{//on suppose que la taille du tableau est inférieure ou égale à n
    return tableau->donnees[n]; //Cette méthode ne renvoie pas la n-ième valeur mais la n+1 mais 
    //comme l'énoncé demande la 5ème valeur pour n=4 je l'ai programmé de cette manière
    //Si l'on veut une fonction qui renvoie la nème valeur il faut remplacer "n" par "n-1"
}
int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->taille;i++){
        if (tableau->donnees[i]==valeur){
            return i+1; // i est l'indice, mais correspond à la (i+1) ème valeur
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if  (tableau->taille<n or n==0){
        std::cout << "Le stockage est impossible"<<endl;
    }
    else{
        tableau->donnees[n-1]=valeur;
    }
}


///J'ai choisi la structure de liste
void pousse_file(Liste* liste, int valeur)
{
    Noeud * newNoeud = new Noeud;
    if (newNoeud==NULL){
        std::cout << "Allocation echec"<<std::endl;
        exit(1);
    }
    newNoeud->donnee=valeur;
    newNoeud->suivant=NULL;
    if (liste->premier==NULL){
        liste->premier=newNoeud;
    }
    else{
    Noeud * actuel = liste->premier;
    while(actuel->suivant!=NULL){
        actuel=actuel->suivant;
    }
    actuel->suivant=newNoeud;
    }
}


int retire_file(Liste* liste)
{
    if(liste->premier->suivant==NULL){
        int valeur=liste->premier->donnee;
        liste->premier=NULL;
        return valeur;
    }
    else{
        Noeud * actuel = liste->premier;
    while(actuel->suivant->suivant!=NULL){
        actuel=actuel->suivant;
    }
    int valeur=actuel->suivant->donnee;
    actuel->suivant=NULL;
    return valeur;
    }
}

void pousse_pile(Liste* liste, int valeur)
{
Noeud *noeud = new Noeud;
    if (noeud==NULL){
        std::cout << "Allocation echec"<<std::endl;
        exit(1);
    }
    noeud->donnee=valeur;
    noeud->suivant=liste->premier;
    liste->premier=noeud;
}

int retire_pile(Liste* liste)
{
    int valeur=liste->premier->donnee;
    liste->premier=liste->premier->suivant;
    return valeur;
}


int main()
{
    Liste liste;
    initialise_liste(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide_liste(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }


    for (int i=1; i<=7; i++) {
        ajoute_liste(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide_liste(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche_liste(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere_liste(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche_liste(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke_liste(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7 à la 4ème place:" << std::endl;
    affiche_liste(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise_liste(&pile);
    initialise_liste(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    std::cout<<"La pile est de : "<<std::endl;
    affiche_liste(&pile);
    std::cout<<"La file est de : "<<std::endl;
    affiche_liste(&file);

    int compteur = 10;
    std::cout << "On retire un à un les éléments de la file (du premier au dernier)" << std::endl;
    while(!est_vide_liste(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }
    
    std::cout << "On retire un à un les éléments de la pile (du dernier au premier)" << std::endl;
    compteur = 10;
    while(!est_vide_liste(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }


    return 0;
}
