#include "tp1.h"
#include <QApplication>
#include <time.h>

void complexe_au_carre(Point * z){
    float a=z->x;
    float b=z->y;
    z->x=a*a-b*b;
    z->y=2*a*b;
}


int isMandelbrot(Point z, int n, Point point){
    complexe_au_carre(&z);
    z.x=z.x+point.x;
    z.y=z.y+point.y;
    float module = sqrt((z.x)*(z.x) + (z.y)*(z.y));
    if ( module > 2 ){
        return n;
    }
    else if (n==0){
        return 0;
    }
    return isMandelbrot(z, n-1, point);
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



