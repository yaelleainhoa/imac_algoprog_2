#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);


int search(int value, Array& toSort, int size)
{
    Context _("search", value, size); // do not care about this, it allow the display of call stack

///Cette configuration marche si value n'est présent qu'une fois dans array, sinon
//elle renvoie l'indice le plus grand où value est présent
//si value n'est pas présent, elle renvoie -1
    if(size==1){
        if(toSort[0]==value){
            return 0;
        }
        else{
            return -1;
        }
    }
    if (toSort[size-1]==value){
        return size-1;
    }
    else{
        return search(value, toSort, size-1);
    }

    return_and_display(-1);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new SearchWindow(search); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}




