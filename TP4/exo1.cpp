#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    if (this->get(nodeIndex)!=NULL){
        return 2*nodeIndex+1;
    }
    return 0;
}

int Heap::rightChild(int nodeIndex)
{
    if(this->get(nodeIndex)!=NULL){
        return 2*nodeIndex+2;
    }
    return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    this->set(i,value);
    while(i>0 && this->get(i)>this->get((i-1)/2)){
        this->swap(i,(i-1)/2);
        i=(i-1)/2;
    }
    
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i_max = nodeIndex;
    int leftChild=this->leftChild(i_max);
    int rightChild=this->rightChild(i_max);
    if(leftChild<heapSize && leftChild!=0 &&  this->get(i_max)<this->get(leftChild)){
        i_max=leftChild;
    }
    if(rightChild<heapSize && rightChild!=0 && this->get(i_max)<this->get(rightChild)){
        i_max=rightChild;
    }
    if(i_max!=nodeIndex){
        this->swap(nodeIndex, i_max);
        this->heapify(heapSize, i_max);
    }
}

///Revoir??
void Heap::buildHeap(Array& numbers)
{
    for(int i=0; i<numbers.size(); i++){
        this->insertHeapNode(i, numbers[i]);
        //this->heapify(numbers.size(),i);
    }

}

void Heap::heapSort()
{
    for(int i=this->size()-1; i>=0; i--){
        this->swap(0,i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
